import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'BlaBla Nick Feng',
      theme: ThemeData(
        primarySwatch: Colors.purple,
        primaryColorLight: Colors.red,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String title = 'BlaBla Nick Feng';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
        centerTitle: true,
        ), 
        body: Center(
          child:Padding(
          padding: const EdgeInsets.all(24.0),  
          child: Container(
            decoration: BoxDecoration(
              border: Border.all(),
          ),
          child: CountryCodePicker(
            initialSelection:'US',
            showCountryOnly: false,
            showOnlyCountryWhenClosed: false,
            favorite: ['+1','US'],
            enabled: true,
            hideMainText: true,
            showFlagMain: true,
            showFlag: true,
            hideSearch: false,
            showFlagDialog: true,
            alignLeft: true,
            padding: EdgeInsets.all(1.0),
          ),
        ),
      ),
    ),
   );
  }
}